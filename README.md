# Api para cadastro de Veículos e Usuários

Essa Api Poderá ser consumida por outros Backends ou por um front end capaz de fazer requisições (React por exemplo)

## Como Utilizar

Certifique-se de que tem o Docker e o docker-compose instalados.
Clone o repositório para uma pasta.
Navegue até a pasta pelo terminal e use o comando:

```
docker-compose up --build -d
```

A Api estará disponível no localhost:8000 (endereço local)

## Crie o Superuser antes de começar a utilizar
Antes de começar a utilizar, rode o comando abaixo para criar o superuser e conseguir acessar o painel ADMIN do
Django pela url localhost:8000/admin
```
docker-compose run web python manage.py createsuperuser
```

## Cadastro de usuários
Fazer o cadastro de usuários pelo Django Admin... se necessário vincular um usuário à um cadastro, pode-se
utilizar o metodo patch da api para usuários.

## Documentação da API

https://documenter.getpostman.com/view/4980345/SW12zHYy?version=latest

## Obervações para uso
Alguns modelos e marcas de veículos serão carregados automaticamente ao subir o sistema.
Caso seja necessário adicionar mais modeos e marcas, fazer pelo django admin.