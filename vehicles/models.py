# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from django.contrib.auth.models import User
import datetime

# Usuarios (registros no sistema)
class PersonRegister(models.Model):
    name = models.CharField(max_length=50)
    document_number = models.CharField(max_length=30)
    id_code = models.CharField(max_length=30)
    user_id = models.ForeignKey(User, 
        on_delete=models.CASCADE,
        blank=True,
        null=True, 
        db_column="user")

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        # Cria o código baseado no id...
        if not self.pk:            
            person = super(PersonRegister, self).save(*args, **kwargs)        
            code = 'P%012d' % self.id         
            self.id_code = code
            self.save()
        else:
            person = super(PersonRegister, self).save(*args, **kwargs)
        return person

# Marcas
class Brand(models.Model):
    name = models.CharField(max_length=30)

    def __str__(self):
        return self.name

# Modelos 
class ModelVehicle(models.Model):
    brand_id = models.ForeignKey(Brand, on_delete=models.SET_NULL, null=True)
    name = models.CharField(max_length=30)

    def __str__(self):
        return "{} {}".format(self.brand_id.name, self.name)

# Veiculos por usuários
class Vehicle(models.Model):
    user_id = models.ForeignKey(PersonRegister, on_delete=models.SET_NULL, null=True, related_name='vehicles')
    # brand_id = models.ForeignKey(Brand, on_delete=models.SET_NULL, null=True)
    model_id = models.ForeignKey(ModelVehicle, on_delete=models.SET_NULL, null=True)
    name = models.CharField(max_length=30, unique=True, default='')
    year_model = models.CharField(max_length=4)
    year_fabric = models.CharField(max_length=4)
    cc = models.CharField(max_length=4, default='')
    plate = models.CharField(max_length=7)

    type_vehicle =  models.CharField(
        max_length=2,
        choices=[('C','Carro'),('M','Moto'),('O','Outro')],
        default='C',
    )

    class Meta:
        ordering = ['name']

    def __str__(self):
        return "{} {} {}/{} PLACA: {}".format(self.name, self.model_id, 
                          self.year_fabric, self.year_model, self.plate)

    def save(self, *args, **kwargs):        
        if not self.pk:
            # Caso esteja criando o veículo, gera um número de matrícula para o mesmo
            vehicle = super(Vehicle, self).save(*args, **kwargs)                    
            code = '%s%012d' % (self.type_vehicle, self.id)
            self.name = code      
            self.save()
        else:
            vehicle = super(Vehicle, self).save(*args, **kwargs)
        return vehicle