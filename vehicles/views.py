# -*- coding: utf-8 -*-
from . import serializers
from rest_framework import generics, status
from rest_framework.response import Response
from . import models
from django.http import Http404
from django.db.models import Q


class PersonRegisterListView(generics.ListAPIView):    
    queryset = models.PersonRegister.objects.all()
    serializer_class = serializers.PersonRegisterSerializerTwo

class PersonRegisterByDocListView(generics.ListAPIView):    
    serializer_class = serializers.PersonRegisterSerializerTwo
    lookup_field = 'doc'

    def get_queryset(self):
        return models.PersonRegister.objects.filter(document_number=self.kwargs['doc'])

class PersonRegisterByCodeListView(generics.ListAPIView):    
    serializer_class = serializers.PersonRegisterSerializerTwo
    lookup_field = 'code'

    def get_queryset(self):                
        return models.PersonRegister.objects.filter(id_code=self.kwargs['code'])

class ModelVehiclesListView(generics.ListAPIView):
    queryset = models.ModelVehicle.objects.all()
    serializer_class = serializers.ModelVehicleSerializer    

class VehiclesListView(generics.ListAPIView):
    # queryset = models.Vehicle.objects.all()
    serializer_class = serializers.VehicleSerializer

    def get_queryset(self):
        # Caso o usuário possua um cadastro de pessoa, mostra apenas os veículos dele
        id_p = self.request.user.id
        person = models.PersonRegister.objects.filter(user_id=id_p)        
        if len(person):            
            return models.Vehicle.objects.filter(user_id=person[0].id)
        else:
            return models.Vehicle.objects.all()


class PersonCreateView(generics.CreateAPIView):
    queryset = models.PersonRegister.objects.all()
    serializer_class = serializers.PersonRegisterSerializer

    def create(self, request, *args, **kwargs):                
        super(PersonCreateView, self).create(request, args, kwargs)        
        response = {"status_code": status.HTTP_200_OK,
                    "message": "Successfully created",
                    "result": request.data}
        return Response(response)

class PersonDetailView(generics.RetrieveUpdateDestroyAPIView):
    queryset = models.PersonRegister.objects.all()
    serializer_class = serializers.PersonRegisterSerializerTwo

    def retrieve(self, request, *args, **kwargs):
        super(PersonDetailView, self).retrieve(request, args, kwargs)        
        instance = self.get_object()
        serializer = self.get_serializer(instance)
        data = serializer.data
        response = {"status_code": status.HTTP_200_OK,
                    "message": "Successfully retrieved",
                    "result": data}
        return Response(response)

    def patch(self, request, *args, **kwargs):
        super(PersonDetailView, self).patch(request, args, kwargs)
        instance = self.get_object()
        serializer = self.get_serializer(instance)
        data = serializer.data
        response = {"status_code": status.HTTP_200_OK,
                    "message": "Successfully updated",
                    "result": data}
        return Response(response)

    def delete(self, request, *args, **kwargs):
        super(PersonDetailView, self).delete(request, args, kwargs)
        response = {"status_code": status.HTTP_200_OK,
                    "message": "Successfully deleted"}
        return Response(response)


class VehiclesListByNameView(generics.ListAPIView):
    queryset = models.Vehicle.objects.all()
    serializer_class = serializers.VehicleSerializer
    lookup_field = 'name'

    def get_queryset(self):
        # Caso o usuário possua um cadastro de pessoa, mostra apenas os veículos dele
        id_p = self.request.user.id
        person = models.PersonRegister.objects.filter(user_id=id_p)        
        if len(person):
            return models.Vehicle.objects.filter(name=self.kwargs['name'], user_id=person[0].id)
        else:
            return models.Vehicle.objects.filter(name=self.kwargs['name'])

class VehiclesListByPlateView(generics.ListAPIView):
    queryset = models.Vehicle.objects.all()
    serializer_class = serializers.VehicleSerializer
    lookup_field = 'plate'

    def get_queryset(self):
        # Caso o usuário possua um cadastro de pessoa, mostra apenas os veículos dele    
        id_p = self.request.user.id
        person = models.PersonRegister.objects.filter(user_id=id_p)        
        if len(person):
            return models.Vehicle.objects.filter(name=self.kwargs['plate'], user_id=person[0].id)
        else:
            return models.Vehicle.objects.filter(name=self.kwargs['plate'])

class VehiclesListByPersonDocView(generics.ListAPIView):
    # queryset = models.Vehicle.objects.all()
    serializer_class = serializers.VehicleSerializer
    lookup_field = 'doc'

    def get_queryset(self):
        # Caso o usuário possua um cadastro de pessoa, mostra apenas os veículos dele
        try:
            id_p = self.request.user.id
            person = models.PersonRegister.objects.filter(user_id=id_p)   
            if len(person):
                user_id = models.PersonRegister.objects.filter(document_number=self.kwargs['doc'], id=person[0].id)
            else: 
                user_id = models.PersonRegister.objects.filter(document_number=self.kwargs['doc'])
            return models.Vehicle.objects.filter(user_id=user_id[0].id)
        except Exception as e:
            response = {
                "status_code": status.HTTP_404_NOT_FOUND,
                "message": "Não foi possível Localizar o veículo "
            }
            return []

class VehicleCreateView(generics.CreateAPIView):
    queryset = models.Vehicle.objects.all()
    serializer_class = serializers.VehicleSerializer

    def create(self, request, *args, **kwargs):        
        # Modificações do Metodo create para facilitar o Input do usuário

        try:
            data_create = request.data
            person_id = models.PersonRegister.objects.filter(document_number=data_create.get('document_number'))
            brand = models.Brand.objects.filter(name=data_create.get('brand_id'))
            model = models.ModelVehicle.objects.filter(name=data_create.get('model_id'), brand_id=brand[0].id)
        except Exception as e:
            # Caso ainda não exista a marca ou o modelo, avisa o client...
            response = {
                "status_code": status.HTTP_400_BAD_REQUEST,
                "message": "Não foi possível criar o Veículo... Verifique se a marca e modelo já existem "
            }
            return Response(response)


        if not len(model):
            response = {
                "status_code": status.HTTP_404_NOT_FOUND,
                "message": "Veiculo não localizado"
            }
        else:
            # Eu uso o Brand para buscar o modelo do Veiculo.. então eu retiro do dict...
            request.data.pop('brand_id')
            request.data.update({'model_id':model[0].id})            
            try:
                if len(person_id):
                    request.data.update({'user_id':person_id[0].id})            
                super(VehicleCreateView, self).create(request, args, kwargs)
                response = {
                    "status_code": status.HTTP_200_OK,
                    "message": "Successfully created",
                    "result": request.data
                }
            except Exception as e:
                print(e)
                response = {
                    "status_code": status.HTTP_400_BAD_REQUEST,
                    "message": "Não foi possível criar o Veículo "
                }


        return Response(response)

class VehicleDetailView(generics.RetrieveUpdateDestroyAPIView):
    queryset = models.Vehicle.objects.all()
    serializer_class = serializers.VehicleSerializer

    def retrieve(self, request, *args, **kwargs):
        super(VehicleDetailView, self).retrieve(request, args, kwargs)        
        instance = self.get_object()
        serializer = self.get_serializer(instance)
        data = serializer.data        
        response = {"status_code": status.HTTP_200_OK,
                    "message": "Successfully retrieved",
                    "result": data}
        return Response(response)

    def patch(self, request, *args, **kwargs):
        super(VehicleDetailView, self).patch(request, args, kwargs)
        instance = self.get_object()
        serializer = self.get_serializer(instance)
        data = serializer.data
        response = {"status_code": status.HTTP_200_OK,
                    "message": "Successfully updated",
                    "result": data}
        return Response(response)

    def delete(self, request, *args, **kwargs):
        super(VehicleDetailView, self).delete(request, args, kwargs)
        response = {"status_code": status.HTTP_200_OK,
                    "message": "Successfully deleted"}
        return Response(response)