from django.conf.urls import url, include
from rest_framework_jwt.views import obtain_jwt_token
from . import views

# Urls para api consumir
urlpatterns = [
    # Usuarios 
    url(r'^login/', obtain_jwt_token),
    url(r'^users/', views.PersonRegisterListView.as_view(), name=None),
    url(r'^create/user', views.PersonCreateView.as_view(), name=None),
    url(r'^user/(?P<pk>\d+)$', views.PersonDetailView.as_view(), name=None),
    url(r'^user/bydoc/(?P<doc>\w+)$', views.PersonRegisterByDocListView.as_view(), name=None),
    url(r'^user/bycode/(?P<code>\w+)$', views.PersonRegisterByCodeListView.as_view(), name=None),
    # Modelos de Carros que podem ser usados para criação
    url(r'^models/', views.ModelVehiclesListView.as_view(), name=None),
    # Veiculos
    url(r'^vehicles/', views.VehiclesListView.as_view(), name='vehicles'),
    url(r'^create/vehicle', views.VehicleCreateView.as_view(), name=None),
    url(r'^vehicle/(?P<pk>\w+)$', views.VehicleDetailView.as_view(), name=None),
    url(r'^vehicle/bycode/(?P<name>\w+)$', views.VehiclesListByNameView.as_view(), name=None),
    url(r'^vehicle/byplate/(?P<plate>\w+)$', views.VehiclesListByPlateView.as_view(), name=None),
    url(r'^vehicle/personDoc/(?P<doc>\w+)$', views.VehiclesListByPersonDocView.as_view(), name=None),
]
