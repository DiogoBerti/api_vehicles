# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from .models import PersonRegister, Brand, ModelVehicle, Vehicle
# Register your models here.

admin.site.register(PersonRegister)
admin.site.register(Brand)
admin.site.register(ModelVehicle)
admin.site.register(Vehicle)
