from . import models
from rest_framework import serializers

class PersonRegisterSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.PersonRegister
        fields = ('name', 'document_number', 'user_id')

class PersonRegisterSerializerTwo(serializers.ModelSerializer):
    vehicles = serializers.StringRelatedField(many=True)

    class Meta:
        model = models.PersonRegister
        fields = ('name', 'document_number','vehicles','id_code')

class ModelVehicleSerializer(serializers.ModelSerializer):
    brand_name = serializers.CharField(read_only=True, source='brand_id.name')
    class Meta:
        model = models.ModelVehicle
        fields = ('brand_name', 'name')

class VehicleSerializer(serializers.ModelSerializer):    
    # Retorna o nome do usuário e a matricula quando lista veículos
    user_name = serializers.CharField(read_only=True, source='user_id.name')
    user_code = serializers.CharField(read_only=True, source='user_id.id_code')
    brand = serializers.CharField(read_only=True, source='model_id.brand_id.name')
    model = serializers.CharField(read_only=True, source='model_id.name')
    

    class Meta:
        model = models.Vehicle
        fields = ('user_id','user_name','user_code','year_model','year_fabric','type_vehicle','brand','model','plate')